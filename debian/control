Source: mediascanner2
Section: libs
Priority: optional
Maintainer: Debian UBports Team <team+ubports@tracker.debian.org>
Uploaders: Mike Gabriel <sunweaver@debian.org>,
           Marius Gripsgard <marius@debian.org>,
Build-Depends: cmake,
               cmake-extras (>= 0.10),
               dbus,
               debhelper-compat (= 13),
               google-mock,
               libapparmor-dev,
               libdbus-1-dev,
               libdbus-cpp-dev (>= 4.0.0),
               libexif-dev,
               libglib2.0-dev,
               libgstreamer-plugins-base1.0-dev,
               libgstreamer1.0-dev,
               libgdk-pixbuf-2.0-dev,
               libgtest-dev,
               libproperties-cpp-dev,
               libsqlite3-dev (>= 3.8.5),
               libtag-dev,
               libudisks2-dev,
               qml-module-qtquick2,
               qml-module-qttest,
               qtbase5-dev,
               qtbase5-dev-tools,
               qtdeclarative5-dev,
               qtdeclarative5-dev-tools,
               dh-apparmor,
# For running unit tests
               gstreamer1.0-plugins-base,
               gstreamer1.0-plugins-good,
               shared-mime-info,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://gitlab.com/ubports/development/core/mediascanner2
Vcs-Git: https://salsa.debian.org/ubports-team/mediascanner2.git
Vcs-Browser: https://salsa.debian.org/ubports-team/mediascanner2

Package: libmediascanner-2.0-4
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Recommends: mediascanner2.0
Description: Access library for the media scanner's index
 This library provides convenient and safe access to the media scanner's
 index files.

Package: mediascanner2.0
Section: misc
Architecture: any
Multi-Arch: foreign
Depends: gstreamer1.0-plugins-base,
         gstreamer1.0-plugins-good,
         dbus,
         ${misc:Depends},
         ${shlibs:Depends},
Description: Media Scanner 2.0
 Media Scanner 2.0 is a user session D-Bus services that scans files (and
 etects changes) in the user's home directory relevant for media apps
 (such as the gallery app, the music player app, the mediaplayer app,
 etc.).
 .
 This package provides the media scanner D-Bus service.

Package: mediascanner2.0-utils
Section: misc
Architecture: any
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends},
Depends: mediascanner2.0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: Media scanner utilities
 Media Scanner 2.0 is a user session D-Bus services that scans files (and
 etects changes) in the user's home directory relevant for media apps
 (such as the gallery app, the music player app, the mediaplayer app,
 etc.).
 .
 This package provides the media scanner utilities for testing and debugging.

Package: libmediascanner-2.0-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libmediascanner-2.0-4 (= ${binary:Version}),
         libsqlite3-dev,
         libglib2.0-dev,
         ${misc:Depends},
Description: Development files for libmediascanner
 Media Scanner 2.0 is a user session D-Bus services that scans files (and
 etects changes) in the user's home directory relevant for media apps
 (such as the gallery app, the music player app, the mediaplayer app,
 etc.).
 .
 This package provides the infrastructure for using the media scanner's
 access library in C++ based projects.

Package: qml-module-mediascanner
Architecture: any
Multi-Arch: same
Depends: mediascanner2.0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Description: QML plugin for the Media Scanner
 Media Scanner 2.0 is a user session D-Bus services that scans files (and
 etects changes) in the user's home directory relevant for media apps
 (such as the gallery app, the music player app, the mediaplayer app,
 etc.).
 .
 This package provides components that allow access to the media
 scanner index from Qt Quick 2 / QML applications.
